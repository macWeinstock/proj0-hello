# Proj0-Hello
-------------

- Author: Mac Weinstock

- Contact: mweinsto@uoregon.edu

- Project0: Repo to print "Hello world" from a config file in order to practice git and very basic software engineering principles.
